package stones.protection.gasdetector;

public class Event {

    private long Gas;
    private long smoke;
    private double Temp;
    private String time;
    private String realtime;


    public Event() {


    }

    public Event(long Gas,long smoke,double Temp,String time,String realtime){

        this.Gas = Gas;
        this.smoke = smoke;
        this.Temp = Temp;
        this.time = time;
        this.realtime = realtime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getTemp() {
        return Temp;
    }

    public void setTemp(double temp) {
        Temp = temp;
    }

    public long getSmoke() {
        return smoke;
    }

    public void setSmoke(long smoke) {
        this.smoke = smoke;
    }

    public long getGas() {
        return Gas;
    }

    public void setGas(long gas) {
        Gas = gas;
    }

    public String getRealtime() {
        return realtime;
    }

    public void setRealtime(String realtime) {
        this.realtime = realtime;
    }
}
