package stones.protection.gasdetector;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class EventListAdapter  extends BaseAdapter {

    Activity activity;
    List<Event> lstEvent;
    LayoutInflater inflater;
    private List<Event> list_stats = new ArrayList<>();
    private Event selectevent;
    Context mContext;
    private String formattedNumber;
    private String service_show;






    public EventListAdapter(Activity activity, List<Event> lstState) {
        this.activity = activity;
        this.lstEvent = lstState;
    }


    @Override
    public int getCount() {
        return lstEvent.size();
    }

    @Override
    public Object getItem(int i) {
        return lstEvent.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        inflater = (LayoutInflater)activity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = convertView;
        if(view == null) {
            view = inflater.inflate(R.layout.list_view_events, null);
        }


        TextView textView_date = (TextView)view.findViewById(R.id.textView_datetime);
        TextView textView_value = (TextView) view.findViewById(R.id.textView_all_value);
        ImageView imageView_stat = (ImageView)view.findViewById(R.id.imageView_photo);

        String path = Environment.getExternalStorageDirectory()
                .toString();



        double temp = lstEvent.get(i).getTemp();
        String TimeStamp = lstEvent.get(i).getTime();
        long  gas = lstEvent.get(i).getGas();
        long smoke = lstEvent.get(i).getSmoke();
        String realtime = lstEvent.get(i).getRealtime();

        String fomat = String.format("%.2f", temp);


        String format_value = "Temp:"+fomat+"°C Gas:"+gas+" smoke:"+smoke;

        textView_date.setText(realtime);
        textView_value.setText(format_value);


        File imgFile = new File(path,"/Afire/"+TimeStamp+".png");

        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            imageView_stat.setImageBitmap(myBitmap);

        }






        return view;
    }








}
