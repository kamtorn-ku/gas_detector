package stones.protection.gasdetector;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;

public class MyService extends Service {

    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("RaspberryApple");
    private TextView textView_temper ;
    private PopupWindow pw;
    Button Close;
    Button Create;

    int i = 0;

    double rate_temp;
    String url;

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "The new Service was Created", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onStart(Intent intent, int startId) {
        // For time consuming an long tasks you can launch a new thread here...
        Toast.makeText(this, " Service Started", Toast.LENGTH_LONG).show();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        SharedPreferences sharedPref= getSharedPreferences("mypref", 0);
        url = sharedPref.getString("url", "http://192.168.0.16:8080/?action=stream");
        rate_temp = sharedPref.getFloat("rate_temp", 40);


        mDatabase.child("state/gas").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                long Value_Gas = (long) dataSnapshot.getValue();
                if(Value_Gas == 0){
                    if(i == 0) {
                        start("warning", "Gas detected !!", 003);
                    }

                    if(i > 5){

                        start("warning", "Gas detected !!", 003);
                        i = 1;
                    }

                    i++;

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mDatabase.child("state/smoke").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                long Value_smoke = (long) dataSnapshot.getValue();
                if(Value_smoke == 0){
                    if(i == 0) {
                        start("warning", "Smoke detected !!", 003);
                    }

                    if(i > 5){

                        start("warning", "Smoke detected !!", 003);
                        i = 1;
                    }

                    i++;

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        mDatabase.child("state/temp").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                double Value = (Double) dataSnapshot.getValue();
              if (Value > rate_temp){

                  if(i == 0){
                      start("warning", "High temperature", 002);
                  }


                  if(i > 4) {
                      start("warning", "High temperature", 002);
                      i = 1;
                  }
                  i++;


//                  Intent intent = new Intent(MyService.this,MainActivity.class);
//                  startActivity(intent);


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();

    }


    void start(String title,String content,int rqcode) {
        try {

            AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

            Date dat = new Date();
            Calendar cal_alarm = Calendar.getInstance();
            Calendar cal_now = Calendar.getInstance();
            cal_now.setTime(dat);


            cal_alarm.setTime(dat);
            cal_alarm.add(Calendar.SECOND,4);









            Intent intent = new Intent(this, AlarmReceiver.class);
            intent.putExtra("extra", content);
            intent.putExtra("extrat", title);
            intent.putExtra("rqcode", rqcode);
            PendingIntent ppi = PendingIntent.getBroadcast(this.getApplicationContext(), rqcode, intent, 0);

            manager.set(AlarmManager.RTC_WAKEUP, cal_alarm.getTimeInMillis(), ppi);




        }catch (Exception e){
            Toast.makeText(MyService.this,"Error Intent",Toast.LENGTH_SHORT).show();
        }

    }



}