package stones.protection.gasdetector;

import android.Manifest;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class MainActivity extends AppCompatActivity {



   //ความเข้าใจเกี่ยวกับ  Activity Life Cycle อันนี้สำคัญครับ 
   // v
   // v
   // v
   // http://www.akexorcist.com/2016/04/why-do-we-need-to-know-about-activity-life-cycle-th.html
   
   

        //เรียกอินสแตนซ์ของฐานข้อมูลโดยใช้ getInstance () และอ้างอิงตำแหน่งที่ต้องการจะเขียน
    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("RaspberryApple");
    
    private Boolean listshow = false; //กำหนดตัวแปร listshow เพื่อใช้ตรวจสอบค่าการแสดงของหน้า list และ camera   
    private ListView list_data; //กำหนดตัวแปร list เพื่อใช้อ้างอิงจาก activity_main.xml
    
    private List<Event> list_event = new ArrayList<>(); //กำหนดตัวแปร จาก class Event เพื่อใช้เรียกฐานข้อมูลโดยเก็บเป็น ArrayList
    private Event selectedEvent; //กำหนดตัวแปร จาก class Event

    private TextView textView_temper,textView_Gas,textView_Smoke; //กำหนดตัวแปร ใช้สำหรับแสดงตัวอักษร TextView ใน MainActivity
    private TextView content_alert; //กำหนดตัวแปร ใช้สำหรับแสดงตัวอักษร TextView ใน Dialog_alert
    private PopupWindow pw;
    private Button btn_resset_alarm;  //กำหนดตัวแปร ใช้สำหรับปุ่ม resetservice
    Button Close;       
    Button Create;        
    
    int CODE_START = 0;    //กำหนดตัวแปรใช้อ้างอิงในการทำงานและแสดง Dialog Warning ที่อยู่บน MainActivity ในกรณีมี Event แจ้งเข้ามาจาก AlarmReciver.class
    String Titled = "";    //กำหนดตัวแปรที่ใช้ในการรับข้อความ ที่ส่งมาจาก Extras Intent  
    String Content = "";  //กำหนดตัวแปรที่ใช้ในการรับข้อความ ที่ส่งมาจาก Extras Intent เพื่อแสดงในหน้า Dialog Warning

   ///////////////////////////////////
   // กำหนดตัวแปรจาก library ใช้สำหรับเรียกแอพลิเคชั่นโดยตรง โดยไม่ต้องปลดล็อค
    PowerManager pm;
    PowerManager.WakeLock wl;
    KeyguardManager km;
    KeyguardManager.KeyguardLock kl;
    Ringtone r;  
    ///////////////////
    
    private WebView webView;  // กำหนดตัวแปรใช้สำหรับแสดงวีดีโอ โดยจะอยู่ในรูปแบบ Webview 
    FileOutputStream fos = null;  //กำหนดตัวแปรของ FileOutputStream  จะใช้ในการเรียก หรือเขียนไฟล์ในเครื่อง
    
    private ConstraintLayout dialog_warning; //กำหนดตัวแปร Layout สำหรับควบคุมการแสดงหน้า Dialog Warning

    private Double Value;    //กำหนดตัวแปร ใช้ในการรับค่าอุณหภูมิ 
    private Long gas_Value;  //กำหนดตัวแปร ใช้ในการรับค่าแก๊ส
    double rate_temp;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPref= getSharedPreferences("mypref", 0);
        url = sharedPref.getString("url", "http://192.168.0.16:8080/?action=stream");
        rate_temp = sharedPref.getFloat("rate_temp", 40);

        ///เรียกการใช้งาน Intent เพื่อรับการส่งข้อมูลมาจาก Class อื่นๆ
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        ////////////////////////////////////////
        
        //อ้างอิงตัวแปร ConstraintLayout จาก  activity_main.xml
        dialog_warning = (ConstraintLayout)findViewById(R.id.dialog_warning);

        dialog_warning.setVisibility(View.GONE); //ซ่อน Dialog_warning

        list_data = (ListView)findViewById(R.id.list_data); //อ้างอิงตัวแปร ListView จาก  activity_main.xml

        list_data.animate().translationX(5000); //เลื่อน list_data ไปทางขวา

        content_alert = (TextView)findViewById(R.id.textView_content_alert); //อ้างอิงตัวแปร TextView จาก  activity_main.xml

        TextView text_alert = (TextView)findViewById(R.id.text_view_setting);




      
        Button btn_view_event =(Button)findViewById(R.id.button_view_event);  //อ้างอิงตัวแปร Button จาก  activity_main.xml
        Button btn_cancel_event = (Button)findViewById(R.id.button_cancel_event); //อ้างอิงตัวแปร Button จาก  activity_main.xml
        btn_resset_alarm = (Button)findViewById(R.id.button_reset_alarm); //อ้างอิงตัวแปร Button จาก  activity_main.xml

        btn_resset_alarm.setVisibility(View.VISIBLE);  // แสดง btn_resset_alarm
        

        ///เพิ่มเติมเกี่ยวกับ Button  ** https://devahoy.com/posts/android-button-onclick-listener-tutorial/
        
        //รับ event จากการกดปุ่ม btn_resset_alarm
        btn_resset_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startService(new Intent(MainActivity.this, MyService.class)); //เริ่ม service background ในคลาส Class  MyService.class

                Toast.makeText(getApplicationContext(),"Alarm has been reset",Toast.LENGTH_LONG).show(); // แสดง pop up ข้อความ

            }
        });

        text_alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,SettingActivity.class);
                startActivity(intent);

            }
        });


        //รับ event จากการกดปุ่ม btn_view_event
        btn_view_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stopService(new Intent(MainActivity.this, MyService.class)); //ทำการหยุด service background ในคลาส Class  MyService.class

                dialog_warning.setVisibility(View.GONE);  // ซ่อน dialog_warning
                btn_resset_alarm.setVisibility(View.VISIBLE); //แสดงปุ่ม btn_resset_alarm
            }
        });
        

        //รับ event จากการกดปุ่ม btn_cancel_event
        btn_cancel_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();   //ปิดหน้า activity ปัจจุบัน
                
                
                //เรียก intent จาก MainActivity.class เพื่อ FLAG ให้ Clear activity ก่อนหน้านี้ออก
                Intent intent = new Intent(getApplicationContext(), MainActivity.class); 
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);  
                startActivity(intent);


            }
        });


        if(extras != null)  // ตรวจสอบว่า extras ว่างหรือไม่  ถ้าว่างให้ทำการตามคำสั่งด้านล่าง
        {
            CODE_START = extras.getInt("CODE_START");  //รับ Value  extras จาก key "CODE_START" ที่ส่งมาจาก Class อื่นๆ
            Titled = extras.getString("TITLE");
            Content = extras.getString("CONTENT");

            content_alert.setText(Content);  // ตั้งข้อความจาก ตัวแปล Content
        }

        if(CODE_START == 112)  // ตรวจสอบ CODE_START  ถ้าเท่ากับ 112 ถ้าว่างให้ทำการตามคำสั่งด้านล่าง 
        { 
            dialog_warning.setVisibility(View.VISIBLE); //แสดง dialog_warning
            btn_resset_alarm.setVisibility(View.GONE);  //ซ่อน dialog_warning
            
            
            
           //เรียก Service เพื่อใช้ในการเรียกแอปพลิเคชั่นขึ้นมา
            pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            km=(KeyguardManager)getSystemService(Context.KEYGUARD_SERVICE);
            kl=km.newKeyguardLock("ShowEvent");
            wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP|PowerManager.ON_AFTER_RELEASE, "ShowEvent");
            wl.acquire(); //wake up the screen
            kl.disableKeyguard();

            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);



        }



        if (Build.VERSION.SDK_INT >= 21)  //ตรวจสอบ version API ของ android
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark)); //ตั้งสีในแถบของ Navigation ฺBar




        textView_temper = (TextView)findViewById(R.id.textView_temp);
        textView_Gas = (TextView)findViewById(R.id.textView_gas_state);
        textView_Smoke = (TextView)findViewById(R.id.textView_smoke_state);
        
        
        
        webView = (WebView) findViewById(R.id.webviewer);  // กำหนด Webview จาก activity_main.xml

        webView.animate().translationX(0);  //ให้ webviewอยู่ในตำแหน่ง 0 หรือ ปัจจุบัน
        
        
        Button button_home = (Button)findViewById(R.id.button);// กำหนด Button จาก activity_main.xml


        //ปุ่ม HOME
        button_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listshow == true)   //ตรวจสอบเงื่อนไขของ listshow ว่าสถานะเป็น true หรือไม่
                {
                    webView.animate().translationX(0);   //ให้ webview อยู่ในตำแหน่ง 0
                    list_data.animate().translationX(5000); //ให้ list_data อยู่ในตำแหน่ง เลื่อนไปทาง ขวา(X) ที่ 5,000
                    listshow = false;   //เปลี่ยนสถานะเป็น false

                }


            }
        });

        //ปุ่ม NOTIFY
        Create = (Button) findViewById(R.id.button2);
        Create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listshow ==false) {   //ตรวจสอบเงื่อนไขของ listshow ว่าสถานะเป็น true หรือไม่
                    webView.animate().translationX(5000); //ให้ webview อยู่ในตำแหน่ง 5000
                    list_data.animate().translationX(0);
                    listshow = true;
                }
            }
        });

        startService(new Intent(this, MyService.class));  //เริ่ม service background

        ///////////  WEBVIEW ใช้ในการแสดง Streaming Camera
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        //กำหนดให้ webview โหลด จาก url
       webView.loadUrl(url);
        //webView.loadUrl("https://www.google.co.th/?hl=th");
        
        
        

        webView.setWebViewClient(new WebViewClient()
        {
        
        // เมื่อโหลดเสร็จให้ทำตามชุดคำสั่ง ใน onPageFinished


            public void onPageFinished(WebView view, String url) {
                if (CODE_START == 112) {
                
                
                //ตั้งชื่อ ไฟลจากการเรียก  System.currentTimeMillis()
                    String name_file = String.valueOf(System.currentTimeMillis());

                //กำหนดขนาดจาก webview และทำการสร้งไฟล์โดย  buildDrawingCache(); จาก webview
                    webView.measure(View.MeasureSpec.makeMeasureSpec(
                            View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED),
                            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

                    webView.layout(0, 0, webView.getMeasuredWidth(),
                            webView.getMeasuredHeight());

                    webView.setDrawingCacheEnabled(true);
                    webView.buildDrawingCache();
                    Bitmap bm = Bitmap.createBitmap(webView.getMeasuredWidth(),
                            webView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

                    Canvas bigcanvas = new Canvas(bm);
                    Paint paint = new Paint();
                    int iHeight = bm.getHeight();
                    bigcanvas.drawBitmap(bm, 0, iHeight, paint);
                    webView.draw(bigcanvas);
                    System.out.println("1111111111111111111111="
                            + bigcanvas.getWidth());
                    System.out.println("22222222222222222222222="
                            + bigcanvas.getHeight());

                    if (bm != null) {
                    
                        try { //หากชุดคำสั่งใน try เกิด error  ให้ไปทำที่ catch
                        
                            String path = Environment.getExternalStorageDirectory()  // กำหนดที่อยู่ของ path
                                    .toString();
                            OutputStream fOut = null;
                            File filecheck = new File(path,"/Afire");   // กำหนดโฟลเดอร์ปลายทาง
                            if (!filecheck.exists()) {  // ตรวจสอบว่ามี โฟลเดอร์หรือไม่ ถ้าไม่มีให้ทำการ  สร้างโฟลเดอร์ โดยใช้คำสั่ง  mkdirs();
                                filecheck.mkdirs();
                            }
                            File file = new File(path, "/Afire/"+name_file+".png");  // กำหนด ชื่อไฟล์ และ โฟลเดอร์ที่ต้องการเก็บ โดยเรียกชื่อไฟล์จาก name_file

                            fOut = new FileOutputStream(file);

                            bm.compress(Bitmap.CompressFormat.PNG, 50, fOut);
                            fOut.flush();
                            fOut.close();
                            bm.recycle();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("MAMMY",e.getMessage());
                            Toast.makeText(getApplicationContext(),"Failed save Shot",Toast.LENGTH_LONG).show();
                        }
                    }


                    final String dateTime[] = getDateTime();
                    String datenow = dateTime[0] + "/"+dateTime[1];

                    Add_Event(datenow,name_file,Value,gas_Value); //เรียกชุดคำสั่ง Add_Event และส่งข้อมูลต่างๆไปให้ Add_Event (บรรทัด 509)

                }
            }
        });


        // ติดตาม ค่าจาก firebase 
        mDatabase.child("state/gas").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                gas_Value = (long) dataSnapshot.getValue(); //Get ค่า Gas จาก firebase และทำการตรวจสอบเงื่อนไข 
                if(gas_Value == 1){
                    textView_Gas.setText("Not\nFound");
                    textView_Smoke.setText("Not\nFound");

                    textView_Gas.setTextColor(Color.parseColor("#05c46b"));
                    textView_Smoke.setTextColor(Color.parseColor("#05c46b"));

                }
                else  if (gas_Value == 0){

                    textView_Smoke.setText("Detected !");
                    textView_Gas.setText("Detected");

                    textView_Gas.setTextColor(Color.parseColor("#ff3f34"));
                    textView_Smoke.setTextColor(Color.parseColor("#ff3f34"));


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        // ติดตาม ค่าอุณหภูมิจาก firebase 
        mDatabase.child("state/temp").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Value = (Double) dataSnapshot.getValue();
                if(Value > 30 && Value < rate_temp){
                    textView_temper.setTextColor(Color.parseColor("#05c46b"));
                }
                if(Value < 30 ){

                    textView_temper.setTextColor(Color.parseColor("#0fbcf9"));

                }

                if (Value > rate_temp){

                    textView_temper.setTextColor(Color.parseColor("#ff3f34"));
                }
                String fomat = String.format("%.2f", Value);
                textView_temper.setText(fomat + "°C");


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        //ทำการ ขอ Permission จากผู้ใช้งาน
        
        if (ContextCompat.checkSelfPermission(MainActivity.this,  // ตรวจสอบว่ามีการ อนุญาติแล้วหรือไม่
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        001);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {








        }



        
        initFirebase();  //เรียกการทำงานของ firebase
        addEventFirebaseListener(); //เรียกการติดตามของ Event ใน Firebase


    }


    // ชุดคำสั่งนี้ไม่ได้ใช้

    private void showPopup() {
        try {
// We need to get the instance of the LayoutInflater

            getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = getLayoutInflater().inflate(R.layout.popup,
                    (ViewGroup) findViewById(R.id.popup_1));
            pw = new PopupWindow(layout, 300, 370, true);
            pw.showAtLocation(layout, Gravity.CENTER, 0, 0);
            Close = (Button) layout.findViewById(R.id.close_popup);
            Close.setOnClickListener(cancel_button);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private View.OnClickListener cancel_button = new View.OnClickListener() {
        public void onClick(View v) {
            pw.dismiss();
        }
    };



        // ใช้ในการเรียกเวลา โดยจะเก็บอยู่ในรูปแบบ array[] 
    private String[] getDateTime() {
        final Calendar c = Calendar.getInstance();
        String dateTime [] = new String[2];
        dateTime[0] = c.get(Calendar.YEAR)+"/"+ (c.get(Calendar.MONTH)+1) +"/"+c.get(Calendar.DAY_OF_MONTH);
        dateTime[1] = c.get(Calendar.HOUR_OF_DAY) +":"+ c.get(Calendar.MINUTE)+":"+ c.get(Calendar.SECOND);
        return dateTime;
    }


                // เพิ่ม Event และส่งไปยัง firebase
  private void Add_Event(final  String realtime,final String time , final double Value, final long Gas){

      mDatabase.child("list_event").addListenerForSingleValueEvent(new ValueEventListener() {
          @Override
          public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
              int count_data = (int)dataSnapshot.getChildrenCount(); // เรียกดูจำนวนข้อมูล เพื่อจัดเรียงตำแหน่ง โดยไม่ให้ข้อมูลเขียนทับกัน

              int ponint = count_data+1; //เพิ่มตัวแปล
              mDatabase.child("list_event/"+ponint+"/realtime").setValue(realtime);
              mDatabase.child("list_event/"+ponint+"/time").setValue(time);
              mDatabase.child("list_event/"+ponint+"/Temp").setValue(Value);
              mDatabase.child("list_event/"+ponint+"/Gas").setValue(Gas);
              mDatabase.child("list_event/"+ponint+"/smoke").setValue(Gas);




          }

          @Override
          public void onCancelled(@NonNull DatabaseError databaseError) {

          }
      });



  }


        // ใช้ในการติดตาม Event ที่เกิดขึ้นบน Firebase 
    private void addEventFirebaseListener() {



        list_data.setVisibility(View.INVISIBLE);
        //progressBar.setVisibility(View.VISIBLE);

        mDatabase.child("list_event").addValueEventListener(new ValueEventListener() { 
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(list_event.size() > 0)   //ตรวจสอบค่่าใน List_Event ว่ามีอยู่หรือไม่
                    list_event.clear();    //ทำการลบค่าออก
                    
                for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){  //เรียกข้อมูลจากจำนวนที่มีอยู่บน Firebase
                    Event event = postSnapshot.getValue(Event.class);  // ทำการ Get ค่าต่างๆมาเก็บไว้ใน Event.class
                    list_event.add(event); // นำค่าต่างๆที่ get มาใส่ใน list_event 

                }
                // กำหนด Adapter  
                //เพิ่มเติม เกี่ยวกับ Listview และ Adapter >>>> http://www.akexorcist.com/2012/09/android-code-custom-list-view.html
                EventListAdapter adapter = new EventListAdapter(MainActivity.this,list_event);
                list_data.setAdapter(adapter);
                Collections.reverse(list_event);

                if(list_event.size() > 0) {
                    //textView_11.setVisibility(View.GONE);

                }else {
                    //textView_11.setVisibility(View.VISIBLE);
                    }


                list_data.setVisibility(View.VISIBLE);
              //  progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
//เริ่มการทำงาน Firebase
    private void initFirebase() {
    
        FirebaseApp.initializeApp(this);
//        mFirebaseDatabase = FirebaseDatabase.getInstance();
//        mDatabaseReference  = mFirebaseDatabase.getReference();
    }




}
