package stones.protection.gasdetector;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);


        SharedPreferences sharedPref= getSharedPreferences("mypref", 0);
        String url = sharedPref.getString("url", "http://192.168.0.16:8080/?action=stream");
        double rate_temp = sharedPref.getFloat("rate_temp", 40);


        final EditText url_input = (EditText)findViewById(R.id.editText);
        final EditText temp_input = (EditText)findViewById(R.id.editText2);
        Button button_con = (Button)findViewById(R.id.button_cf);

        url_input.setText(url);
        temp_input.setText(String.valueOf(rate_temp));

        button_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url_set = url_input.getText().toString();
                Float set_value = Float.valueOf(temp_input.getText().toString());


                SharedPreferences sharedPref= getSharedPreferences("mypref", 0);
                //now get Editor
                SharedPreferences.Editor editor= sharedPref.edit();
                //put your value
                editor.putString("url", url_set);
                editor.putFloat("rate_temp", set_value);
                //commits your edits
                editor.commit();

                Toast.makeText(SettingActivity.this,"successful",Toast.LENGTH_SHORT).show();

                finish();


            }
        });





    }




}
